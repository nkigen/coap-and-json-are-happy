
#include "json-data.h"
#include "contiki.h"
#include "json.h"  

  int16_t lim=25421;
  static long a = 1; 
 const char alphanum[] = 
	 "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

 
/**
 * 
 */
int16_t next_val()
{
 
     // could be made the seed value
        a = (a * 32719 + 3) % 32749; 
	int16_t r= ((a % lim) + 1);
	return r;
	
}
/*
 unsigned next_val()
  {
     unsigned short lfsr = 0xACE1u;
  unsigned bit;
    bit  = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
    return lfsr =  (lfsr >> 1) | (bit << 15);
  }
  
  */

/**
 * 
 * 
 */
char
getrand_boolean()
{
  if((uint16_t)next_val()>next_val()/2)
    return JSON_TYPE_FALSE;
  return JSON_TYPE_TRUE;
}


/**
 * generates an integer
 **/
void 
getrand_number(int16_t *num)
{
  int16_t n=(uint16_t)next_val();
  if(next_val()>next_val()/2)
    *num=next_val();
  else
    *num=n;
  return;
}


/**
 * 
 * 
 */
void 
getrand_string(char *str, int8_t len)
{

    int8_t i;
    for (i = 0; i < len; ++i) {
        str[i] = alphanum[next_val()*next_val() % (sizeof(alphanum) - 1)];
    }

    str[len] = '\0';  
}

