/*
 * Copyright (c) 2012, Matthias Kovatsch and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Erbium (Er) REST Engine example (with CoAP-specific code)
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdio.h> 
#include <string.h>
#include<stdlib.h>
#include "contiki.h"
#include "contiki-net.h"
#include "jsontree.h"
#include "jsonparse.h"
#include "project-json.h"
/* Define which resources to include to meet memory constraints. */

#define REST_RES_CHUNKS 1
#define REST_RES_HELLO 1


#if !UIP_CONF_IPV6_RPL && !defined (CONTIKI_TARGET_MINIMAL_NET) && !defined (CONTIKI_TARGET_NATIVE)
#warning "Compiling with static routing!"
#include "static-routing.h"
#endif

#include "erbium.h"


/* For CoAP-specific example: not required for normal RESTful Web service. */
#if WITH_COAP == 3
#include "er-coap-03.h"
#elif WITH_COAP == 7
#include "er-coap-07.h"
#elif WITH_COAP == 12
#include "er-coap-12.h"
#elif WITH_COAP == 13
#include "er-coap-13.h"
#else
#warning "Erbium example without CoAP-specifc functionality"
#endif /* CoAP-specific example */

#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif


/*global variables definition*/

char outbuffer[BUFFER_SIZE];
//int bpos=0;
int bpos_out=0;
struct jsontree_context json;
uint8_t rcounter;
static struct jsontree_string author_name = JSONTREE_STRING("nelson kigen");
static struct jsontree_string author_email = JSONTREE_STRING("nellyk89@gmail.com");

/*tree definition*/


/**
 * 
 */
int 
output_boolean(struct jsontree_context *ctx)
{
  char c[6];
  c[0]=getrand_boolean();
  if(*c=='f')
  {
    c[1]='a';
    c[2]='l';
    c[3]='s';
    c[4]='e';
   c[5]='\0';
  }
  else if(*c=='t')
  {
     c[1]='r';
    c[2]='u';
    c[3]='e';
    c[4]='\0';
    
  }
  
  jsontree_write_string(ctx,c);
  return 0;
}

/**
 * 
 * 
 */
int
output_number(struct jsontree_context *ctx)
{
  int num;
  getrand_number(&num);
  jsontree_write_int(ctx,num);
  return 0;
}


/**
 * 
 * 
 */
int
output_string(struct jsontree_context *ctx)
{
  int8_t len=7;
  char temp[len];
  getrand_string(temp,len);
  jsontree_write_string(ctx,temp);
  return 0;
}





int output_array(struct jsontree_context *ctx)
{
 int16_t val;
 getrand_number(&val);
 jsontree_write_int(ctx,val);
  
  return 0;
}


/*json tree array definition*/
//struct jsontree_int rnd_int[ARRAY_SIZE];
//struct jsontree_string rnd_str[ARRAY_SIZE]; 
/*
struct jsontree_value *rnd_values[ARRAY_SIZE];
struct jsontree_array rnd_array={JSON_TYPE_ARRAY,ARRAY_SIZE,rnd_values};
*/
JSONTREE_ARRAY(rnd_array,ARRAY_SIZE);

static struct jsontree_callback string_callback = JSONTREE_CALLBACK(output_string, NULL);
static struct jsontree_callback number_callback = JSONTREE_CALLBACK(output_number, NULL);
static struct jsontree_callback boolean_callback = JSONTREE_CALLBACK(output_boolean, NULL);
static struct jsontree_callback array_callback = JSONTREE_CALLBACK(output_array, NULL);
 


JSONTREE_OBJECT( author_obj,
		JSONTREE_PAIR("name",&author_name),
		JSONTREE_PAIR("email",&author_email));


JSONTREE_OBJECT( object_obj,
		 JSONTREE_PAIR("boolean",&boolean_callback),
		 JSONTREE_PAIR("string",&string_callback),
		 JSONTREE_PAIR("number",&number_callback));  
		 

JSONTREE_OBJECT( data_obj, 
		JSONTREE_PAIR("object",&object_obj),  
		JSONTREE_PAIR("array",&rnd_array));

JSONTREE_OBJECT(tree,
		JSONTREE_PAIR("data",&data_obj),
		JSONTREE_PAIR("author",&author_obj)
	      );
 

 

/******************************************************************************/
#if REST_RES_HELLO
/*
 * Resources are defined by the RESOURCE macro.
 * Signature: resource name, the RESTful methods it handles, and its URI path (omitting the leading slash).
 */
RESOURCE(helloworld, METHOD_GET, "hello", "title=\"Hello world: ?len=0..\";rt=\"Text\"");

/*
 * A handler function named [resource name]_handler must be implemented for each RESOURCE.
 * A buffer for the response payload is provided through the buffer pointer. Simple resources can ignore
 * preferred_size and offset, but must respect the REST_MAX_CHUNK_SIZE limit for the buffer.
 * If a smaller block size is requested for CoAP, the REST framework automatically splits the data.
 */
void
helloworld_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  printf("hello-request received\n");
  const char *len = NULL;
  /* Some data that has the length up to REST_MAX_CHUNK_SIZE. For more, see the chunk resource. */
  //char const * const message = "Hello World! ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxy";
  int length = 80; /*           |<-------->| */
  send_json();
  int i=0;
  printf("begin\n");
  while(i<BUFFER_SIZE){
      printf("%c ",outbuffer[i]);
      i++;
  }
  printf("end\n");
  // printf("(%.*s)\n", BUFFER_SIZE, outbuffer);
  /* The query string can be retrieved by rest_get_query() or parsed for its key-value pairs. */
  if (REST.get_query_variable(request, "len", &len)) {
    length = atoi(len);
    if (length<0) length = 0;
    if (length>REST_MAX_CHUNK_SIZE) length = REST_MAX_CHUNK_SIZE;
    memcpy(buffer, outbuffer, length);
     // memcpy(buffer, message, length);
  } else {
   // memcpy(buffer, message, length);
    memcpy(buffer, outbuffer, length);
  }

  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_header_etag(response, (uint8_t *) &length, 1);
  REST.set_response_payload(response, buffer, length);
}
#endif

/******************************************************************************/
#if REST_RES_CHUNKS
/*
 * For data larger than REST_MAX_CHUNK_SIZE (e.g., stored in flash) resources must be aware of the buffer limitation
 * and split their responses by themselves. To transfer the complete resource through a TCP stream or CoAP's blockwise transfer,
 * the byte offset where to continue is provided to the handler as int32_t pointer.
 * These chunk-wise resources must set the offset value to its new position or -1 of the end is reached.
 * (The offset for CoAP's blockwise transfer can go up to 2'147'481'600 = ~2047 M for block size 2048 (reduced to 1024 in observe-03.)
 */
RESOURCE(chunks, METHOD_GET, "json_server", "title=\"JSON SERVER\";rt=\"Data\"");

#define CHUNKS_TOTAL    bpos_out

void
chunks_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  int32_t strpos = 0;
  if(*offset==0)
    send_json();
 // printf("preffered size %d **",preferred_size); 
    printf("\nchunks-request received offset %ld\n",*offset);
 
  /* Check the offset for boundaries of the resource data. */
  if (*offset>=CHUNKS_TOTAL)
  {
    REST.set_response_status(response, REST.status.BAD_OPTION);
    /* A block error message should not exceed the minimum block size (16). */

    const char *error_msg = "BlockOutOfScope";
    REST.set_response_payload(response, error_msg, strlen(error_msg));
    ++rcounter;
    return;
  }

  /* Generate data until reaching CHUNKS_TOTAL. */
  while (strpos<preferred_size && (CHUNKS_TOTAL-*offset)>=strpos)
  {
    strpos += snprintf((char *)buffer+strpos, preferred_size-strpos+1, "%c", *(outbuffer+strpos+*offset));
  }

  /* snprintf() does not adjust return value if truncated by size. */
  if (strpos > preferred_size)
  {
    strpos = preferred_size;
  }

  /* Truncate if above CHUNKS_TOTAL bytes. */
  if (*offset+(int32_t)strpos > CHUNKS_TOTAL)
  {
    strpos = CHUNKS_TOTAL - *offset;
  }
  

  REST.set_response_payload(response, buffer, strpos);

  /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
  *offset += strpos;
 
  /* Signal end of resource representation. */
  if (*offset>=CHUNKS_TOTAL)
  {
    ++rcounter;
    *offset = -1;
  }
  
}
#endif



PROCESS(rest_server_example, "Erbium Example Server");
AUTOSTART_PROCESSES(&rest_server_example);

PROCESS_THREAD(rest_server_example, ev, data)
{
  PROCESS_BEGIN();

  PRINTF("Starting Erbium Example Server\n");

#ifdef RF_CHANNEL
  PRINTF("RF channel: %u\n", RF_CHANNEL);
#endif
#ifdef IEEE802154_PANID
  PRINTF("PAN ID: 0x%04X\n", IEEE802154_PANID);
#endif

  PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
  PRINTF("LL header: %u\n", UIP_LLH_LEN);
  PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
  PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);

/* if static routes are used rather than RPL */
#if !UIP_CONF_IPV6_RPL && !defined (CONTIKI_TARGET_MINIMAL_NET) && !defined (CONTIKI_TARGET_NATIVE)
  set_global_address();
  configure_routing();
#endif

  /* Initialize the REST engine. */
  rest_init_engine();
  init_json();
  /* Activate the application-specific resources. */

  #if REST_RES_HELLO
  rest_activate_resource(&resource_helloworld);
#endif
#if REST_RES_CHUNKS
  rest_activate_resource(&resource_chunks);
#endif

  /* Define application-specific events here. */
  while(1) {
    PROCESS_WAIT_EVENT();
  } /* while (1) */

  PROCESS_END();
}


void init_json()
{
  printf("server-jsontree_context init...");
  json.putchar=putchar_out;
  json.values[0] = (struct jsontree_value *)&tree;
  bpos_out=0;
  
  jsontree_array_init(&rnd_array,&array_callback);
  /*
  int i=0;
  for(i=0;i<ARRAY_SIZE;++i)
  { 
    (rnd_array.values)[i]=(struct jsontree_value *)&array_callback;
  }
  
  */
}

int putchar_out(int c)
{
  if(bpos_out<BUFFER_SIZE){
  outbuffer[bpos_out++]=c;
  return c; 
  }
  return 0;
}

void send_json()
{
 bpos_out=0;
jsontree_reset(&json);
while(jsontree_print_next(&json) && json.path <= json.depth);
}


/*extra*/

void 
jsontree_array_init(struct jsontree_array *array,struct jsontree_callback *cback)
{
  int count=array->count;
  while(count>=0){
    array->values[count]=(struct jsontree_value *)cback;
    count--;
  }
}
