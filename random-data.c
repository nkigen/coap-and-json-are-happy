#include "contiki.h"
#include<math.h>
#include<limits.h>
#include<stdio.h>
#include "random-data.h"
#include "jsontree.h"


static unsigned int iseed=19726;

/*
 generates the random data
 */
static int 
getrandom_data(jsontree_context *ctx)
{
  
  return 0;
}

/*random data callback */
static struct jsontree_callback data_callback=
  JSONTREE_CALLBACK(getrandom_data, NULL);

  /*json tree*/
JSON_OBJECT(tree,
	    JSONTREE_PAIR("data",&data_callback));

	    



void 
getrandom_string(char *string,int len)
{
  srand(iseed);
  int i=0;
  while(i<len){
    *(string+i)= 'a'+(int)(MAX_CHAR*(rand()/(RAND_MAX+1.0)));
    ++i;
  }
}


int 
getrandom_int()
{
  srand(iseed);
  return (int)(INT_MAX*(rand()/(RAND_MAX+1.0)));
}

int 
getrandom_datatype()
{
  srand(iseed);
  return (int)(NUM_DATATYPES*(rand()/(RAND_MAX+1.0)));
}