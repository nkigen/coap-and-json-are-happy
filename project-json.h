#ifndef __PROJECT_JSON_
#define __PROJECT_JSON_

#include "jsontree.h"
#include "json-data.h"
#define BUFFER_SIZE 200
#define MAXL 10

void init_json();
void send_json();
int putchar_out(int c);

void jsontree_array_init(struct jsontree_array *array,struct jsontree_callback *cback);



#define JSONTREE_ARRAY(name,count) \
static struct jsontree_value *jsontree_value##name[count]; \
static struct jsontree_array name={JSON_TYPE_ARRAY,count,jsontree_value##name}


  
  
#endif