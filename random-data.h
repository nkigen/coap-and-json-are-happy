#define RANDOM_DATA_
#ifndef RANDOM_DATA_

#include "contiki.h"
#include "jsontree.h"
#define MAX_LETTERS ('z'-'a')

enum
{
  INTEGER,
  STRING,
  ARRAY,
  OBJECT,
  NUM_DATATYPES
}RANDOM_DATATYPE;

int getrandom_datatype();
void getrandom_string(char *string,int len);
int getrandom_int();



#endif RANDOM_DATA_