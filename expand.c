
#define JSONTREE_ARRAY(name,items,type)				\
static struct jsontree_value *jsontree_value##name[items];			\
int jsontree_array##name=0;						\
if(type==JSONTREE_INT){							\
 static struct jsontree_int jsontree_int##name[items];			\
for(jsontree_array##name=0;i<items;++i){				\
  jsontree_int##name.type=JSON_TYPE_INT;				\
  jsontree_value##name[i]=(struct jsontree_value*)jsontree_int##name;}}	\
if(type==JSONTREE_STRING){							\
 static struct jsontree_string jsontree_string##name[items];		\
 for(jsontree_array##name=0;i<items;++i){				\
   jsontree_string##name.type=JSON_TYPE_STRING;			\
  jsontree_value##name[i]=(struct jsontree_value*)jsontree_string##name;}}\
  static struct jsontree_array name={JSON_TYPE_ARRAY,items		\
  ,jsontree_value##name};						