#ifndef __CLIENT_JSON_
#define __CLIENT_JSON_

#include "jsonparse.h"
 #define BUFFER_SIZE 200
#define MAXL 10
#define ARRAY_SIZE 5
void init_json(const char *buf,int len);
void process_json();
void object_handler(struct jsonparse_state *parser,char *name); 
#endif